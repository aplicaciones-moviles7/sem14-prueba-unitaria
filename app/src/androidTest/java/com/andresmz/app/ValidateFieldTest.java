package com.andresmz.app;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import androidx.test.ext.junit.rules.ActivityScenarioRule;

import org.junit.Rule;
import org.junit.Test;

public class ValidateFieldTest {

    private static final String email_1 = "email1@gmail.com";
    private static final String email_2 = "email2.com";
    private static final String email_3 = "";

    private static final String password_1 = "";
    private static final String password_2 = "pass";
    private static final String password_3 = "pass";

    @Rule
    public ActivityScenarioRule<MainActivity> mActivityTestRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void primera(){
        onView(withId(R.id.fieldEmail)).perform(typeText(email_1),closeSoftKeyboard());
        onView(withId(R.id.fieldPassword)).perform(typeText(password_1),closeSoftKeyboard());
        onView(withId(R.id.button_login)).perform(click());
    }


    @Test
    public void segundo(){
        onView(withId(R.id.fieldEmail)).perform(typeText(email_2),closeSoftKeyboard());
        onView(withId(R.id.fieldPassword)).perform(typeText(password_2),closeSoftKeyboard());
        onView(withId(R.id.button_login)).perform(click());
    }


    @Test
    public void tercero(){
        onView(withId(R.id.fieldEmail)).perform(typeText(email_3),closeSoftKeyboard());
        onView(withId(R.id.fieldPassword)).perform(typeText(password_3),closeSoftKeyboard());
        onView(withId(R.id.button_login)).perform(click());
    }


}
