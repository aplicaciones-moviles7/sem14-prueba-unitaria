package com.andresmz.app.Validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateFields {

    public static boolean email(String textField){

        Pattern pattern = Pattern.compile("/[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}/g", Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(textField);

        return textField.isEmpty() || matcher.find();

    }

    public static boolean password(String textField){
        return !textField.isEmpty();
    }

}
