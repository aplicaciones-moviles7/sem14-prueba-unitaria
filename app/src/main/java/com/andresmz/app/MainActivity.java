package com.andresmz.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andresmz.app.Validate.ValidateFields;

public class MainActivity extends AppCompatActivity {

    EditText fieldEmail, fieldPassword;
    Button buttonSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fieldEmail = findViewById(R.id.fieldEmail);
        fieldPassword = findViewById(R.id.fieldPassword);
        buttonSignIn = findViewById(R.id.button_login);

        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ValidateFields.email(fieldEmail.getText().toString())){

                    if(ValidateFields.password(fieldPassword.getText().toString())){
                        Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(getBaseContext(), "La contraseña es requerida", Toast.LENGTH_SHORT).show();
                    }

                }else{
                    Toast.makeText(getBaseContext(), "EL email es requerido", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}